import {Game} from "../Game";
import {makeLogger} from "ts-loader/dist/logger";

export class InputController {
  game: Game;

  controlState: ControlState;

  click: any;
  keyPress: any;

  constructor(game: Game, onClick?, onKey?) {
    this.game = game;

    this.click = onClick;
    this.keyPress = onKey;

    this.controlState = {
      keyState: {
        left: false,
        right: false,
        up: false,
        down: false,
      },
      pointer: {
        x: 0,
        y: 0,
        active: false
      }
    };


    this.init()
  }

  init() {
    this.game.screen.ctx.canvas.addEventListener('mousemove', (e) => {
      this.controlState.pointer.x = e.offsetX;
      this.controlState.pointer.y = e.offsetY
    });
    this.game.screen.ctx.canvas.addEventListener('mousedown', (e) => {
      this.controlState.pointer.active = true
    });
    this.game.screen.ctx.canvas.addEventListener('mouseup', (e) => {
      this.controlState.pointer.active = false
    });

    this.game.screen.ctx.canvas.addEventListener('click', (e) => {
      this.click(e)
    });

    window.addEventListener('keydown', (e) => {
      this.keyPress(e.keyCode);
      switch (e.keyCode) {
        case 65: // left
          this.controlState.keyState.left = true;
          break;
        case 68: // right
          this.controlState.keyState.right = true;
          break;
        case 87: // up
          this.controlState.keyState.up = true;
          break;
        case 83: // down
          this.controlState.keyState.down = true;
          break
      }
      // console.log('keyState', keyState)
    });
    window.addEventListener('keyup', (e) => {
      // console.log('keyup', e.keyCode);
      switch (e.keyCode) {
        case 65: // left
          this.controlState.keyState.left = false;
          break;
        case 68: // right
          this.controlState.keyState.right = false;
          break;
        case 87: // up
          this.controlState.keyState.up = false;
          break;
        case 83: // down
          this.controlState.keyState.down = false;
          break;
        case 32: // space
          this.click(e)
      }
      // console.log('keyState', keyState)
    })
  }
}

export interface ControlState {
  keyState: {
    left: boolean,
    right: boolean,
    up: boolean,
    down: boolean,
  },
  pointer: {
    x: number,
    y: number,
    active: boolean
  }
}

export interface KeyState {
  left: boolean,
  right: boolean,
  up: boolean,
  down: boolean,
}

export interface Pointer {
  x: number,
  y: number,
  active: boolean
}
