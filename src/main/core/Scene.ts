import {Game} from "../Game";

export class Scene {
    game: Game;

    constructor(game: Game) {
        this.game = game;
    }

    update(time: number) {

    }

    render(time: number) {

    }
}