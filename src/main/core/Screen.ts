import {ImageLoader} from "./ImageLoader";


export class Screen {
  width: number;
  height: number;
  canvas: HTMLCanvasElement;
  images: any;
  isImagesLoaded = false;
  ctx: CanvasRenderingContext2D;


  constructor(width = 600, height = 600) {
    this.width = width;
    this.height = height;
    this.canvas = this.createCanvas(width, height);
    this.ctx = this.canvas.getContext('2d');
    this.images = {};
  }

  createCanvas(width, height) {
    let elements = document.getElementsByTagName('canvas');
    let canvas = elements[0] || document.createElement('canvas');
    document.body.appendChild(canvas);
    canvas.width = width;
    canvas.height = height;
    return canvas;
  }

  loadImages(imageFiles) {
    const loader = new ImageLoader(imageFiles);
    loader.load().then((names) => {
      this.images = Object.assign(this.images, loader.images);
      this.isImagesLoaded = true;
    });
  }

  clear() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height)
  }

  drawLine(p1, p2) {
    this.ctx.beginPath();
    this.ctx.strokeStyle = '#ccc';
    this.ctx.moveTo(p1.x, p1.y);
    this.ctx.lineTo(p2.x, p2.y);
    this.ctx.closePath();
    this.ctx.stroke();
  }

  drawPoint(point: any) {
    this.ctx.save();
    this.ctx.beginPath();
    this.ctx.fillStyle = '#ccc';
    this.ctx.arc(point.x, point.y, 2, 0, Math.PI * 2);
    this.ctx.closePath();
    this.ctx.fill();
    this.ctx.restore()
  }


  drawText(text, x, y) {
    this.ctx.save();
    this.ctx.fillStyle = 'black';
    this.ctx.font = '12px sans-serif';
    this.ctx.fillText(text, x, y);
    this.ctx.restore()
  }
}
