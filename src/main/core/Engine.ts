import {Game} from "../Game";

export class Engine {
  game: any;
  last: number;
  step = 1 / 60;
  dt = 0;
  now: number;
  update: any;
  render: any;
  totalTime = 0;

  constructor(game: Game, update, render) {
    this.game = game;

    this.update = update;
    this.render = render;

    this.last = Date.now();
    this.now = Date.now();
  }

  init() {
    window.requestAnimationFrame(this.loop.bind(this))
  }

  loop() {
    let now = Date.now();
    this.dt = this.dt + Math.min(1, (now - this.last) / 1000);

    while (this.dt > this.step) {
      this.dt = this.dt - this.step;
      this.update(this.step);
    }


    this.last = now;
    this.render(this.dt);
    this.totalTime += this.dt;

    window.requestAnimationFrame(this.loop.bind(this))
  }
}
