import {Game} from "./Game";
import {Point} from "./libs/Point";

export class CursorManager {
  point: Point;
  game: Game;
  constructor(game: Game) {
    this.game = game;
  }

  addPoint(x, y) {
    if (!this.point) {
      this.point = new Point(x, y)
    } else {
      this.point.x = x;
      this.point.y = y;
    }
  }

  removePoint() {
    this.point = null
  }

  getPoint(): Point | null {
    return this.point ? this.point : null;
  }

}
