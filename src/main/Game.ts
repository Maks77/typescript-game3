import {Engine} from "./core/Engine";
import {Screen} from "./core/Screen";
import {InputController} from "./core/InputController";
import {Scene} from "./core/Scene";
import {Level1} from "./scenes/Level1";
import {CursorManager} from "./CursorManager";

export class Game {
  engine: Engine;
  screen: Screen;
  inputController: InputController;
  cursorManager: CursorManager;

  activeSceneIndex = 0;
  scenesList: Scene[] = [];

  constructor() {
    this.engine = new Engine(this, this.update.bind(this), this.render.bind(this));
    this.screen = new Screen(600, 600);
    this.inputController = new InputController(this, this.onClick.bind(this), this.onKeyPress.bind(this));

    this.scenesList.push(
        new Level1(this)
    );

    this.cursorManager = new CursorManager(this)
  }

  getActiveScene() {
    return this.scenesList[this.activeSceneIndex]
  }

  start() {
    this.engine.init();
  }

  update(time) {
    this.getActiveScene().update(time);
  }

  render(time) {
    this.screen.clear();
    this.getActiveScene().render(time);
  }

  onKeyPress(keyCode) {
    console.log(keyCode);
  }

  onClick(e) {
    console.log('click');
    this.cursorManager.addPoint(e.clientX, e.clientY)
  }
}

export interface Obstacle {
  x: number;
  y: number;
  w: number;
  h: number;
}
