export class Vector{
  x: number;
  y: number;

  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  setXY(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  add(vector: Vector) {
    return new Vector(this.x + vector.x, this.y + vector.y)
  }

  subtract(vector: Vector) {
    return new Vector(this.x -= vector.x, this.y -= vector.y)
  }

  multiply(n: number) {
    return new Vector(this.x *= n, this.y *= n);
  }

  length() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  cross(vector: Vector) {
    return this.x * vector.y - this.y * vector.x;
  }

  dot(vector: Vector) {
    return this.x * vector.x + this.y * vector.y
  }

  normalize() {
    return new Vector(this.x / this.length(), this.y / this.length())
  }

}
