import {Vector} from "./Vector";

export class Point {
  x: number;
  y: number;

  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  draw(ctx: CanvasRenderingContext2D, radius?, color?) {
    radius = radius ? radius : 2;
    color = color ? color : '#ccc';
    ctx.beginPath();
    ctx.fillStyle = color;
    ctx.arc(this.x, this.y, radius, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
  }

  distanceTo(point: Point) {
    let dx = this.x - point.x;
    let dy = this.y - point.y;
    return Math.sqrt(dx * dx + dy * dy);
  }

  getVectorFrom(p) {
    return new Vector(this.x - p.x, this.y - p.y);
  }
}
