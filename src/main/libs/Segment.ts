import {Point} from "./Point";

export class Segment {
  x: number;
  y: number;
  vecx: number;
  vecy: number;

  constructor(x, y, vecx, vecy) {
    this.x = x;
    this.y = y;
    this.vecx = vecx;
    this.vecy = vecy;
  }

  draw(ctx: CanvasRenderingContext2D, width?, color?) {
    ctx.beginPath();
    ctx.lineWidth = width || 1;
    ctx.moveTo(this.x, this.y);
    ctx.lineTo(this.x + this.vecx, this.y + this.vecy);
    ctx.strokeStyle = color || '#ccc';
    ctx.stroke();
  }

  length() {
    const dx = this.x - this.x + this.vecx;
    const dy = this.y - this.y + this.vecy;
    return Math.sqrt(dx * dx + dy * dy)
  }

  normal() {
    const x1 = this.y;
    const y1 = this.x + this.vecx;
    const y2 = this.x;
    const x2 = this.y + this.vecy;
    return new Segment(x1, y1, x2 - x1, y2-y1);
  }

  center() {
    let _x = this.x + this.x + this.vecx;
    let _y = this.y + this.y + this.vecy;
    _x /= 2;
    _y /= 2;
    return new Point(_x, _y);
  }

  intersects(segment: Segment) {
    const pointA = new Point(this.x, this.y);
    const pointB = new Point(this.x + this.vecx, this.y + this.vecy);
    const pointC = new Point(segment.x, segment.y);
    const pointD = new Point(segment.x + segment.vecx, segment.y + segment.vecy);

    const z1 = (pointA.x - pointB.x);
    const z2 = (pointC.x - pointD.x);
    const z3 = (pointA.y - pointB.y);
    const z4 = (pointC.y - pointD.y);
    const dist = z1 * z4 - z3 * z2;
    if (dist == 0) {
      return null;
    }
    const tempA = (pointA.x * pointB.y - pointA.y * pointB.x);
    const tempB = (pointC.x * pointD.y - pointC.y * pointD.x);
    const xCoor = (tempA * z2 - z1 * tempB) / dist;
    const yCoor = (tempA * z4 - z3 * tempB) / dist;

    if (xCoor < Math.min(pointA.x, pointB.x) || xCoor > Math.max(pointA.x, pointB.x) ||
      xCoor < Math.min(pointC.x, pointD.x) || xCoor > Math.max(pointC.x, pointD.x)) {
      return null;
    }
    if (yCoor < Math.min(pointA.y, pointB.y) || yCoor > Math.max(pointA.y, pointB.y) ||
      yCoor < Math.min(pointC.y, pointD.y) || yCoor > Math.max(pointC.y, pointD.y)) {
      return null;
    }

    return new Point(xCoor, yCoor);
  }
}
